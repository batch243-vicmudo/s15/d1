// console.log ("Hello World");

// [Section] Syntax, Statements and comments
// Statements in programming are instructions that we tell the computer to perform.
// Js statements usually end with semicolon(;).
// Semicolons are not required in JS, but we will use it to help us train to locate where a statements ends.
// A syntax in programming, it is the set of rules of rules that we describe how statements must be construted.
// All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in certain manner.

// Comments are parts of the code that gets ignored by the language.
// Comments are meant to describe the written code.


/*
	There are two types of comments:
	1. The single-line comment denoted by two slashes.
	2. The multi-line comment denoted by slash and asterisk
*/

// [Section] Variables

// Variables are use to contain data.
// Any information that is used by an application is stored in what we call the "memory".
// When we create variables, certain portions of a device's memory is given a name that we call variables.
// This makes it easier for us to associate information stored in our devices to actual "names" about information.

// Declaring variables - it tells our devices that a variable name is created and is ready to store data.
//Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined
	// syntax
		// let /const variableName;
'use strict';
// example of declaration
let myVariable = "Ada Lovelace";
// const myVariable = "Ada Lovelace";
// console.log() is useful for printing values of variables or certain results of code into the Google Chrome browser's console.
// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code.
console.log(myVariable);

/*
Guides in writing variables:
	1. use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
	2. Variable names should start with lowercase character, use camelCase for multiple words.
	3. For constant variable, use the 'const' keyword.
		using let: we can change the value of the variable.
		using const: we cannot change the value of the variable.
	4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion. 
*/

// Declare and initialize varibles
// 	Initializing variables - the instance when a variable is given it's initial/ starting value
	// syntax
		// let/const variableName = value;
let	productName ="desktop computer";

console.log(productName);
let	productPrice =18999;

console.log(productPrice);

	// In the context of certain applications, some variables / information are constant and should not be changed.
	// In this example, the interest rate for a loan, saving account or a mortgage must not be change due to real world concerns.
const interest = 3.539;

console.log(interest);

// Reassigning variable values
// Reassigning a variable means changing it's initial or previous value into another value.
	// syntax
	// variableName = newValue;

productName= "laptop";
console.log(productName);
console.log(productName);

//error
// let productName = "laptop";
// console.log(productName)
	//let variable - cannnot be re-declared within its scope.

// values of constants cannot be changed and will simply return an error.
// interest = 4.489;
// console.log(interest);

// Reassigning variables and initializing variables

let supplier;
//Initialization
supplier = "Zuitt Store";
// Reassigning
supplier = "Zuitt merch";

const name= "George Alfred Cabaccang";
//error name = "George Alfred Cabaccang";

// var vs let/const
	// some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see var.
	// var - is also used in declaring variable. But var is an EcmaScript1 feature [ES1 (Javascript 1997)]
	// let/const was introduced as new feature in ES6 (2016)

	// what makes let/const different var?

a=5;
console.log(a)
var a;

/*b = 6;
console.log(b)
let b;*/

// let/const local/global scope 
// scope is essentially means where these variable are available for use
// let and const are block scoped.
// A block is a chunk of code bounded by {}. A block in curly braces. Anything within the curly braces is a block.

let outerVariable = "hello from the other side";

{
	let innerVarible = "Hello from the block";
	console.log(innerVarible);
	console.log(outerVariable);
}

{
	let innerVarible = "hello from the second block"
	console.log(innerVarible);
}

console.log(outerVariable);
//console.log(innerVarible);

//multiple variable declarations
// multiplt variables can be declared in one line.

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);
console.log(productCode);
console.log(productBrand);

// using a variable with a reverse keyword

/*
const let = "hello";
console.log(let);
*/

//[Sections] Data Types
// Strings - are series of characters that create a word, a phrase, a sentence or anything related to creating text.
// Strings in JavaScript can be written using either single (') and double ("")quote.
// In other programming languages, only the double can be used for creating string.

let country = "Philippines";
let province = 'Metro Manila';

// Concatenate - Mulitple string values can be combined to create single string using the "+" symbol.

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The escape characters (\) in strings in combination with other characters can produce different effects.

// "\n" refers to creating a new line between text 
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early.";

//message = 'John\'s employees went home early.';
console.log(message);

//numbers
// Intergers/whole numbers
let count = "26";
let headcount = 26;
console.log(count);
console.log(headcount);

// Decimal Numbers / Fracions
let grade = 98.7;
console.log(grade);

//exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is "+ grade);
console.log(count + headcount);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things.
//This will be useful in further discussions about creating logic to make our application respond to certain scenarios.

let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);

console.log("isMarried: "+isMarried);

// Arrays - are a special kind of data type that's used to store multiple values.
// Arrays can store different data types but is normally use store similar data types.

	// Similar data types
		// Syntax
		// let/const arrayName = [elementA, elementB, elementC . . .]

let grades = [98.7, 92.1, 90.8, 94.6]
console.log(grades[1]);
console.log(grades);

	//different data types
		//storing different data types inside an array is not recommended because it will not make sense in the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects - are another special kind of data type that's used to mimic real world objects/items
//they're used to create a complex data that contains pieces of information that is relevant

//syntax:
	//let/const objectName = {
	// propertyA: Value,
	// propertyB: value,
//};

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0974836478", "3746392"],
	address:{
		houseNumber: '434',
		city: "Manila"
	}
};

console.log(person);
console.log(person.age);
console.log(person.contact[1]);
console.log(person.address.city);

// typeof operator is used o determine the type of data or the value of a variable.

console.log(typeof person);
// Note: Array is a special type of object with methods and functions to manipulate it. We will discuss these methods in later sessions. (S22 Javascript - array manipulation).
console.log(typeof details);

	/*
	        Constant Objects and Arrays
	            The keyword const is a little misleading.

	            It does not define a constant value. It defines a constant reference to a value.

	            Because of this you can NOT:

	            Reassign a constant value
	            Reassign a constant array
	            Reassign a constant object

	            But you CAN:

	            Change the elements of constant array
	            Change the properties of constant object

	        */
	const anime= ['one piece', 'one punch man', 'attack on titans'];
	// anime = ['kimetsu no yaiba'] - error
console.log(anime);
	anime[0] = 'Kimetsu no yaiba'
console.log(anime);
	anime[4] = 'dxd';
	console.log(anime);
// Null
// It is used to intionally expressed the absence of a value in a variable/ initialization.
// Null simply mean that a data type was assigned to a variable but it does not hold any value/amount or is nullified.
let spouse = null;
console.log(spouse);

//undefined
//represents the state of a variable 

let fullName;
console.log(fullName);

// undefined vs Null
// The difference between undefined and nulls is that for undefined, a variable was created but not provide a value.
// null means that a variable was created and was assigned a value that does not hold any value/ amount.